# Preparação do Ambiente.

Para se executar a aplicação Web, deve estar instalado o xampp ou wampp no seu desktop. Este funcionará como um "emulador" do banco e para quaisquer arquivos criados para uma aplicação Web. Para acessar a página no seu browser localhost/index.php, classe principal que irá plotar no mapa os agentes ativos no momento, e caso o usúario entre com um endereço, retorna-se uma lista de agentes que podem resolver o determinado problema com base na descrição, retornando sempre o mais próximo do local. 

Temos também uma aplicação que foi desenvolvida em React Native na qual disponibilizamos o as coordenadas para o banco de dados. Essa aplicação seria instalada nos smartphones dos agentes sendo que eles precisariam ter conectividade com a internet para a sincronização de dados e recebimento de tarefas. Uma aplicação simples que se dispõe em somente uma tela. 

# Solução Proposta.

O grupo desenvolveu uma aplicação web que a priori plota todos os agentes cadastrados no banco de dados no mapa, observa-se que para cada agente tem-se uma cor referente. 
Na aplicação Web o usuario tem a opção de entrar com um endereço para selecionar os agentes mais proximos daquele local. O sistema envia um alerta para o dispositivo móvel do agente mais próximo da ocorrência recebida, e este agente aparece no mapa com seu nome código de identificação e telefone.

